//
//  RocketParametrCell.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import UIKit

// MARK: - Model for cell in ViewController
class RocketParametrCell: UICollectionViewCell {
    @IBOutlet var metricLabel: UILabel!
    @IBOutlet var metricDescriptionLabel: UILabel!
    
}
