//
//  Constants.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import Foundation

// SpaceX API's URL
let rocketsDataURLString = "https://api.spacexdata.com/v4/rockets"
let rocketsLounchesURLString = "https://api.spacexdata.com/v4/launches"

// UserDefaults keys for measureements
var keys = ["height", "diameter", "mass", "payloadWeights"]

// UserDefaults keys for segmentControls
var indexSegmentKeys = ["heightIndex", "diameterIndex", "massIndex", "payloadWeightsIndex"]
