//
//  UserDefaultsManager.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 17.04.2022.
//

import Foundation

struct UserDefaultsManager {
    func fetchMeasurements(completion: @escaping (String?, String?, Error?) -> Void) {
        let defaults = UserDefaults.standard
        
        for key in keys {
            if let jsonData = defaults.object(forKey: key) as? Data {
                let jsonDecoder = JSONDecoder()
                DispatchQueue.global(qos: .userInitiated).async {
                    do {
                        switch key {
                        case "height":
                            completion("height", try jsonDecoder.decode(String.self, from: jsonData), nil)
                        case "diameter":
                            completion("diameter", try jsonDecoder.decode(String.self, from: jsonData), nil)
                        case "mass":
                            completion("mass", try jsonDecoder.decode(String.self, from: jsonData), nil)
                        case "payloadWeights":
                            completion("payloadWeights", try jsonDecoder.decode(String.self, from: jsonData), nil)
                        default:
                            break
                        }
                    } catch let error {
                        completion(nil, nil, error)
                    }
                }
            }
        }
    }
    
    func fetchIndexesFromUserDefaults(completion: @escaping (String?, Int?, Error?) -> Void) {
        let defaults = UserDefaults.standard
        
        DispatchQueue.global(qos: .userInitiated).async {
            for key in indexSegmentKeys {
                if let jsonData = defaults.object(forKey: key) as? Data {
                    let jsonDecoder = JSONDecoder()
                    DispatchQueue.main.async {
                        
                        do {
                            switch key {
                            case "heightIndex":
                                completion("heightIndex", try jsonDecoder.decode(Int.self, from: jsonData), nil)
                            case "diameterIndex":
                                completion("diameterIndex", try jsonDecoder.decode(Int.self, from: jsonData), nil)
                            case "massIndex":
                                completion("massIndex", try jsonDecoder.decode(Int.self, from: jsonData), nil)
                            case "payloadWeightsIndex":
                                completion("payloadWeightsIndex", try jsonDecoder.decode(Int.self, from: jsonData), nil)
                            default:
                                break
                            }
                        } catch let error {
                            completion(nil, nil, error)
                        }
                    }
                }
            }
        }
    }
    
    func saveInUserDeafults<T: Encodable>(element: T, key: String) {
        let jsonEncoder = JSONEncoder()
        
        if let savedData = try? jsonEncoder.encode(element) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: key)
        }
    }
}
