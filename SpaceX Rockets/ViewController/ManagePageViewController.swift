//
//  ManagePageViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import UIKit

class ManagePageViewController: UIPageViewController {
    var currentIndex: Int!
    var rockets: [RocketData]?
    
    let networkManager: NetworkManager = NetworkManager()
    let userDefaultsManager: UserDefaultsManager = UserDefaultsManager()
    
    // MARK: - measurements for rocket's parametrs
    var currentHight: String!
    var currentDiameter: String!
    var currentMass: String!
    var currentPayloadWeight: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let viewController = viewRocketInfoController(currentIndex ?? 0) {
            let viewControllers = [viewController]
            
            setViewControllers(viewControllers, direction: .forward, animated: false)
        }
        
        dataSource = self
    }
}

// MARK: - Create VCs and fill it with data
extension ManagePageViewController {
    func viewRocketInfoController(_ index: Int) -> ViewController? {
        guard let storyboard = storyboard,
              let page = storyboard
            .instantiateViewController(withIdentifier: "ViewController") as? ViewController else { return nil }
        
        networkManager.fetchRocketData(from: rocketsDataURLString) { [weak self] (rocketsResponse: [RocketData]?, error) in
            rocketsResponse.map { [weak self] rockets in
                let rocket = rockets[index]
                
                var formattedData: String {
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-mm-dd"
                    
                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "dd MMM, yyyy"
                    
                    let date: Date? = dateFormatterGet.date(from: rocket.firstFlight)
                    
                    return String(describing: dateFormatterPrint.string(from: date!))
                }
                
                self?.rockets = rockets
                page.rocket = rockets[index]
                page.pageIndex = index
                
                page.rocketNameLabel.text = rocket.name
                page.dateLaunchLabel.text = formattedData
                
                if rocket.country == "United States" {
                    page.launchCountryLabel.text = "США"
                } else {
                    page.launchCountryLabel.text = "М. Острова"
                }
                
                page.costLaunchLabel.text = "$\(rocket.costPerLaunch / 1_000_000) млн"
                
                page.firstNumberOfPartsLabel.text = "\(rocket.firstStage.engines)"
                page.firstFuelQuantityLabel.text = "\(rocket.firstStage.fuelAmountTons)"
                page.firstTimeToBurnLabel.text = "\(rocket.firstStage.burnTimeSEC ?? 0)"
                
                page.secondNumberOfPartsLabel.text = "\(rocket.secondStage.engines)"
                page.secondFuelQuantityLabel.text = "\(rocket.secondStage.fuelAmountTons)"
                page.secondTimeToBurnLabel.text = "\(rocket.secondStage.burnTimeSEC ?? 0)"
                
                page.rocketParametrsCollectionView.reloadData()
                }
        }
        
        return page
    }

}

// MARK: - change VC right after changing measurements
extension ManagePageViewController {
    override func viewWillAppear(_ animated: Bool) {
        fetchCurrentMeasurements()
    }
}

// MARK: - Fetch measurements for CollectionView in ViewController
extension ManagePageViewController {
    func fetchCurrentMeasurements() {
        userDefaultsManager.fetchMeasurements { [weak self] (mesurement, currentMesurement, error) in
            switch mesurement {
            case "height":
                self?.currentHight = currentMesurement
            case "diameter":
                self?.currentDiameter = currentMesurement
            case "mass":
                self?.currentMass = currentMesurement
            case "payloadWeights":
                self?.currentPayloadWeight = currentMesurement
            default:
                break
            }
        }
    }
}

// MARK: - Implement funcs for PageViewControllerDataSource 
extension ManagePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let viewController = viewController as? ViewController,
           let index = viewController.pageIndex, index > 0 {
            return viewRocketInfoController(index - 1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let viewController = viewController as? ViewController,
           let index = viewController.pageIndex,
           (index + 1) < rockets?.count ?? 0 {
            return viewRocketInfoController(index + 1)
        }
        
        return nil
    }
}


// MARK: - Displaying a Page Control Indicator
extension ManagePageViewController {
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 4
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return currentIndex ?? 0
    }
}
