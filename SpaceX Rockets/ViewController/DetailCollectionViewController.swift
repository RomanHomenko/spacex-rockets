//
//  DetailCollectionViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 15.04.2022.
//

import UIKit


class DetailCollectionViewController: UICollectionViewController {
    let networkManager = NetworkManager()
    var rocketLaunches: [RocketInSpaceData] = []
    var rocketID: String = ""
    var rocketName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = rocketName
        navigationController?.isNavigationBarHidden = false
    }
}

// MARK: - Setting collection view
extension DetailCollectionViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rocketLaunches.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = cellSettings(for: rocketLaunches[indexPath.row], indexPath: indexPath)
        
        return cell
    }
}

// MARK: - individual Cell settings
extension DetailCollectionViewController {
    func cellSettings(for launch: RocketInSpaceData, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rocketLaunch", for: indexPath) as! RocketLaunchCell

        cell.layer.cornerRadius = 30
        
        var formattedData: String {
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.locale = Locale(identifier: "en_US_POSIX")
            dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd MMM, yyyy"
            
            let date: Date? = dateFormatterGet.date(from: launch.dateUTC)
            
            return String(describing: dateFormatterPrint.string(from: date!))
        }
        
        // chek for rocket will be in space or already had been in space
        guard let successfulLaunch = launch.success else {
            cell.successfulLaunch.image = UIImage(named: "rocketWillBeLaunch")
            
            cell.dateLaunchLabel.text = formattedData
            cell.rocketNameLabel.text = launch.name

            return cell
        }
        cell.successfulLaunch.image = UIImage(named: successfulLaunch ? "rocketGood" : "rocketBad")
        
        cell.dateLaunchLabel.text = formattedData
        cell.rocketNameLabel.text = launch.name
        
        return cell
    }
}

// MARK: - fetch RocketInSpaceData when VC willAppear
extension DetailCollectionViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = false
        
        networkManager.fetchRocketData(from: rocketsLounchesURLString) { [weak self] (launches: [RocketInSpaceData]?, error) in
            launches?.forEach({ launch in
                if self?.rocketID == launch.rocket.rawValue {
                    self?.rocketLaunches.append(launch)
                }
            })
            self?.rocketLaunches.reverse()
            self?.collectionView.reloadData()
        }
    }
}

// MARK: - Hidee navBar when VC is willDisappear
extension DetailCollectionViewController {
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
}
