//
//  ViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import UIKit
import UserNotifications

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    // MARK: - Full and detail info about Rocket
    @IBOutlet var detailContentView: UIView!
    
    // MARK: - Rocket name
    @IBOutlet var rocketNameLabel: UILabel!
    
    // MARK: - Shoud use to popUp settings later
    @IBOutlet var settingsButton: UIButton!
    
    // MARK: - Random rocketImage
    // use Bundle to get access for assets later
    @IBOutlet var rocketImageView: UIImageView!
    
    // MARK: - CollectionView instance
    @IBOutlet var rocketParametrsCollectionView: UICollectionView!
    
    // MARK: - Info about Rockets' Past
    @IBOutlet var dateLaunchLabel: UILabel!
    @IBOutlet var launchCountryLabel: UILabel!
    @IBOutlet var costLaunchLabel: UILabel!
    
    
    // MARK: Info First Step Labels
    @IBOutlet var firstNumberOfPartsLabel: UILabel!
    @IBOutlet var firstFuelQuantityLabel: UILabel!
    @IBOutlet var firstTimeToBurnLabel: UILabel!
    
    // MARK: Info Second Step Labels
    @IBOutlet var secondNumberOfPartsLabel: UILabel!
    @IBOutlet var secondFuelQuantityLabel: UILabel!
    @IBOutlet var secondTimeToBurnLabel: UILabel!
    
    // MARK: - Button to see all rocket launches
    @IBOutlet var rocketLaunchesButton: UIButton!
    
    // MARK: - One rocket from JSON to display
    var rocket: RocketData?
    
    // MARK: - Current index of Page
    var pageIndex: Int!
    
    // MARK: - measurements for rocket's parametrs    
    var currentHight: String!
    var currentDiameter: String!
    var currentMass: String!
    var currentPayloadWeight: String!
    
    // MARK: - Make haptic vibration
    let vibration = HapticManager.shared
    
    // MARK: - For interaction with notifications' functionality
    let notification = Notification()
    
    // MARK: - Fetch images
    let networkManager = NetworkManager()
    
    // MARK: - Fetch measurements
    let userDefaultsManager = UserDefaultsManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        downloadImageFromURL()
        fetchCurrentMeasurements()
        
        // seend notification
        notification.askPermissionToSendNotifications()
        notification.sendNotification()
        
        viewSettings()
    }
    
    // MARK: - activate haptic vibration
    @IBAction func hapticVibrationTapped(_ sender: Any) {
        vibration.selectionVibrate()
    }
    
}

extension ViewController {
    func viewSettings() {
        detailContentView.roundCorners([.topLeft, .topRight], radius: 45)
        dateLaunchLabel.sizeToFit()
        settingsButton.setTitle("", for: .normal)
        rocketLaunchesButton.layer.cornerRadius = 15
        navigationController?.isNavigationBarHidden = true
    }
}

// MARK: - Add protocol stubs for UICollectionViewDataSource and setting Cells

extension ViewController {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RocketParametrCell
        
        // Create four different models for this
        switch indexPath.row {
        case 0:
            cell.metricLabel.text = "\(String(describing: currentHight=="m" ? rocket?.height.meters ?? 0 : rocket?.height.feet ?? 0))"
            cell.metricDescriptionLabel.text = "высота, \(currentHight ?? "m")"
        case 1:
            cell.metricLabel.text = "\(String(describing: currentDiameter=="m" ? rocket?.diameter.meters ?? 0 : rocket?.diameter.feet ?? 0))"
            cell.metricDescriptionLabel.text = "диаметр, \(currentDiameter ?? "m")"
        case 2:
            cell.metricLabel.text = "\(String(describing: currentMass=="kg" ? rocket?.mass.kg ?? 0: rocket?.mass.lb ?? 0))"
            cell.metricDescriptionLabel.text = "масса, \(currentMass ?? "kg")"
        case 3:
            cell.metricLabel.text = "\(String(describing: currentPayloadWeight=="kg" ? rocket?.payloadWeights.first?.kg ?? 0: rocket?.payloadWeights.first?.lb ?? 0))"
            cell.metricDescriptionLabel.text = "нагрузка, \(currentPayloadWeight ?? "kg")"
        default:
            break
        }
        
        cell.layer.cornerRadius = 40
        
        return cell
    }
}

// MARK: - UIView extensions

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}


// MARK: - Fetch Images from API's URL

extension ViewController {
    func downloadImageFromURL() {
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            if self?.rocket == nil {
                self?.downloadImageFromURL()
            }
            self?.networkManager.feetchImageFromURL(urlAdress: self?.rocket?.flickrImages.randomElement() ?? "") { [weak self] image in
                self?.rocketImageView.image = image
            }
        }
    }
}

// MARK: - Work with segues
extension ViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "rocketLaunches" {
            let rocketName = rocket?.name ?? ""
            let rocketID = rocket?.id ?? ""
    
            let detailVC = segue.destination as! DetailCollectionViewController
            detailVC.rocketName = rocketName
            detailVC.rocketID = rocketID
        }
        if segue.identifier == "settings" {
            // Implement delegate pattern
            let settingsVC = segue.destination as! SettingsViewController
            settingsVC.delegate = self
        }
    }
}

extension ViewController {
    func fetchCurrentMeasurements() {
        userDefaultsManager.fetchMeasurements { [weak self] (mesurement, currentMesurement, error) in
            switch mesurement {
            case "height":
                self?.currentHight = currentMesurement
            case "diameter":
                self?.currentDiameter = currentMesurement
            case "mass":
                self?.currentMass = currentMesurement
            case "payloadWeights":
                self?.currentPayloadWeight = currentMesurement
            default:
                break
            }
        }
    }
}

// MARK: - Implement delegate function
extension ViewController: ViewControllerDelegate {
    func reloadRocketInfo() {
        fetchCurrentMeasurements()

        rocketParametrsCollectionView.reloadData()
    }
}
