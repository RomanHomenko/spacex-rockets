//
//  SettingsViewController.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 16.04.2022.
//

import UIKit

// MARK: - Create deelegate protocol
protocol ViewControllerDelegate: AnyObject {
    func reloadRocketInfo()
}

class SettingsViewController: UIViewController {

    @IBOutlet var heightSegmet: UISegmentedControl!
    @IBOutlet var diameterSegment: UISegmentedControl!
    @IBOutlet var massSegment: UISegmentedControl!
    @IBOutlet var payloadWeightsSegment: UISegmentedControl!
    
    // MARK: - Measureements
    var currentHight: String!
    var currentDiameter: String!
    var currentMass: String!
    var currentPayloadWeights: String!
    
    weak var delegate: ViewControllerDelegate?
    
    let vibration = HapticManager.shared
    
    let userDefaultsManager = UserDefaultsManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        heightSegmet.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
        diameterSegment.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
        massSegment.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
        payloadWeightsSegment.addTarget(self, action: #selector(changeMeasurements), for: .valueChanged)
    }
    
    // MARK: - reload collection view in main ViewController by delegate pattern
    @IBAction func dismissViewControllerButton(_ sender: UIButton) {
        delegate?.reloadRocketInfo()
        
        vibration.selectionVibrate()
        
        dismiss(animated: true)
    }
    
    // MARK: - activate vibration after change segment
    @IBAction func activateVibrationTapped(_ sender: Any) {
        vibration.selectionVibrate()
    }
}

// MARK: - Change measurements when segmentControl value is change and save segmentControle in UseerDefaults
extension SettingsViewController {
    @objc func changeMeasurements(target: UISegmentedControl) {
        let selectedIndex = target.selectedSegmentIndex
        switch target {
        case heightSegmet:
            setMeasurements(for: "height", target.titleForSegment(at: selectedIndex) ?? "")
            saveInUserDefaults(element: selectedIndex, key: "heightIndex")
        case diameterSegment:
            setMeasurements(for: "diameter", target.titleForSegment(at: selectedIndex) ?? "")
            saveInUserDefaults(element: selectedIndex, key: "diameterIndex")
        case massSegment:
            setMeasurements(for: "mass", target.titleForSegment(at: selectedIndex) ?? "")
            saveInUserDefaults(element: selectedIndex, key: "massIndex")
        case payloadWeightsSegment:
            setMeasurements(for: "payloadWeights", target.titleForSegment(at: selectedIndex) ?? "")
            saveInUserDefaults(element: selectedIndex, key: "payloadWeightsIndex")
        default:
            break
        }
    }
}

// MARK: - Set value measurements and save it to UserDefaults
extension SettingsViewController {
    func setMeasurements(for parametr: String,_ measurement: String) {
        switch parametr {
        case "height":
            currentHight = measurement
            saveInUserDefaults(element: currentHight, key: "height")
        case "diameter":
            currentDiameter = measurement
            saveInUserDefaults(element: currentDiameter, key: "diameter")
        case "mass":
            currentMass = measurement
            saveInUserDefaults(element: currentMass, key: "mass")
        case "payloadWeights":
            currentPayloadWeights = measurement
            saveInUserDefaults(element: currentPayloadWeights, key: "payloadWeights")
        default:
            break
        }
    }
}

// MARK: - Should save String and Int values in UserDefaults
extension SettingsViewController {
    func saveInUserDefaults<T: Encodable>(element: T, key: String) {
        userDefaultsManager.saveInUserDeafults(element: element, key: key)
    }
    
    // MARK: - Frtch indexes for segmentCotrols from UserDefaults
    func fetchIndexesFromUserDefaults() {
        userDefaultsManager.fetchIndexesFromUserDefaults { [weak self] (segmentName, index, error) in
            if let error = error {
                print(String(describing: error))
            }
            switch segmentName {
            case "heightIndex":
                self?.heightSegmet.selectedSegmentIndex = index ?? 0
            case "diameterIndex":
                self?.diameterSegment.selectedSegmentIndex = index ?? 0
            case "massIndex":
                self?.massSegment.selectedSegmentIndex = index ?? 0
            case "payloadWeightsIndex":
                self?.payloadWeightsSegment.selectedSegmentIndex = index ?? 0
            default:
                break
            }
        }
    }
}

// MARK: - Fetch segmentControls indexes from UserDefaults when viewWillAppear
extension SettingsViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchIndexesFromUserDefaults()
    }
}
