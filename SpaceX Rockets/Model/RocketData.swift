//
//  RocketData.swift
//  SpaceX Rockets
//
//  Created by Роман Хоменко on 14.04.2022.
//

import Foundation

// MARK: - Rocket Info
class RocketData: Decodable {
    // MARK: - Data for collection view description
    let height, diameter: Diameter
    let mass: Mass
    let firstStage: FirstStage
    let secondStage: SecondStage

    
    let firstFlight, country: String
    let costPerLaunch: Int
    
    // let engines: Engines
    let payloadWeights: [PayloadWeight]
    let flickrImages: [String]
    let name, type: String
    let id: String

    enum CodingKeys: String, CodingKey {
        case height, diameter, mass
        case payloadWeights = "payload_weights"
        
        case firstStage = "first_stage"
        case secondStage = "second_stage"
        
        // case engines
        case flickrImages = "flickr_images"
        case name, type
        case costPerLaunch = "cost_per_launch"
        // case successRatePct = "success_rate_pct"
        case firstFlight = "first_flight"
        case country
        case id
    }
}

// MARK: - Engines
struct Engines: Decodable {
    let number: Int

    enum CodingKeys: String, CodingKey {
        case number
    }
}

// MARK: - FirstStage
struct FirstStage: Decodable {
    let engines: Int
    let fuelAmountTons: Double
    let burnTimeSEC: Int?

    enum CodingKeys: String, CodingKey {
        case engines
        case fuelAmountTons = "fuel_amount_tons"
        case burnTimeSEC = "burn_time_sec"
    }
}

// MARK: - SecondStage
struct SecondStage: Decodable {
    let engines: Int
    let fuelAmountTons: Double
    let burnTimeSEC: Int?

    enum CodingKeys: String, CodingKey {
        case engines
        case fuelAmountTons = "fuel_amount_tons"
        case burnTimeSEC = "burn_time_sec"
    }
}

// MARK: - Mass
struct Mass: Decodable {
    let kg, lb: Int
}

// MARK: - PayloadWeight
struct PayloadWeight: Decodable {
    let kg, lb: Int
}


// MARK: - Diameter
struct Diameter: Decodable {
    let meters, feet: Double?
}
